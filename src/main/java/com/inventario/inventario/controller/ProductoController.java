package com.inventario.inventario.controller;

import org.aspectj.bridge.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.inventario.inventario.model.Producto;
import com.inventario.inventario.service.ProductoService;


@RestController
@CrossOrigin
@RequestMapping(value = "/V1/producto")
public class ProductoController {

	@Autowired
	ProductoService productoService;
	
	@RequestMapping(method=RequestMethod.GET)
	public  ResponseEntity<Object>listarProductos(){
	return new ResponseEntity<Object>(productoService.mostrarProductos(), HttpStatus.OK);}
	
	@RequestMapping(method=RequestMethod.POST)
	public  ResponseEntity<Object>guardarProducto(@RequestBody Producto producto){
	return new ResponseEntity<Object>(productoService.guardarProducto(producto), HttpStatus.OK);}
	
	@RequestMapping(method=RequestMethod.GET, path="/categorias")
	public ResponseEntity<Object> listarCategorias(){
		return new ResponseEntity<Object> (productoService.mostrarCategorias(), HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.PUT, path = "/{id}")
	public ResponseEntity<Object>editarProducto(@PathVariable(value = "id")  Long id, @RequestBody Producto producto){
		productoService.editarProducto(id, producto);
		return new ResponseEntity<Object> (producto,HttpStatus.OK);
	}
	
	
	@RequestMapping(method=RequestMethod.DELETE,path="/{id}")
	public ResponseEntity<String> eliminarProducto(@PathVariable(value="id") Long id){
		productoService.eliminarProducto(id);

		return new ResponseEntity<String> ( "Objeto eliminado", HttpStatus.OK);
	}
}

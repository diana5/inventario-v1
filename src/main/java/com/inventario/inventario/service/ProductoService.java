package com.inventario.inventario.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.inventario.inventario.model.Categoria;
import com.inventario.inventario.model.Producto;
import com.inventario.inventario.repository.CategoriaRepository;
import com.inventario.inventario.repository.ProductoRepository;

@Service
public class ProductoService {

	@Autowired
	ProductoRepository productoRepository;
	
	@Autowired
	CategoriaRepository categoriaRepository;
	
	public Producto guardarProducto(Producto producto) {
		productoRepository.save(producto);
		return producto; }
	
	
	public Producto editarProducto(Long id, Producto producto) {
		Optional<Producto> productoexistente=productoRepository.findById(id);
		if (!productoexistente.isPresent())
			return new Producto();
		
		productoexistente.get().setCantidad(producto.getCantidad());
		productoexistente.get().setDescripcion(producto.getDescripcion());
		productoexistente.get().setFecha(producto.getFecha());
		productoexistente.get().setNombre(producto.getNombre());
		productoexistente.get().setCategoria(producto.getCategoria());
		productoRepository.save(productoexistente.get());
		return producto;
		
	}
	
	public void eliminarProducto(Long id) {
		productoRepository.deleteById(id);
		
	}
	
	public List<Producto> mostrarProductos(){
		return productoRepository.findAll();
	}
	
	public Optional<Producto> detalleProducto(Long id) {
		return productoRepository.findById(id); 
	
	}
	
	public List<Categoria> mostrarCategorias(){
		return categoriaRepository.findAll();
	}
	
	
	
	
}

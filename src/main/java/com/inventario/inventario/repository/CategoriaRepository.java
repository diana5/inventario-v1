package com.inventario.inventario.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.inventario.inventario.model.Categoria;

public interface CategoriaRepository extends JpaRepository<Categoria,Long>{

}
